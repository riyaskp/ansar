<?php

use yii\db\Migration;

/**
 * Class m180407_183505_all_tables
 */
class m180407_183505_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'code' => $this->string(55)->notNull(),
            'name' => $this->string(55)->notNull(),
            'mobile' => $this->string(15)->notNull(),
            'email' => $this->string(55)->null(),
            'rank' => $this->string(55)->notNull(),
            'specialist' => $this->string(55)->notNull(),
            'office' => $this->string(55)->notNull(),
            'shift' => $this->string(55)->notNull(),
            'joining_date' => $this->date(),
            'promotion_date' => $this->date(),
            'joining_date' => $this->date(),
            'promotion_date' => $this->date(),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->createTable('absent', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer(11)->notNull(),
            'absent_type' => $this->string(55)->notNull(),
            'holiday_type' => $this->string(55)->notNull(),
			'course_type' => $this->string(15)->notNull(),
			'leave_type' => $this->string(15)->notNull(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'place' => $this->string(55)->null(),
            'comment' => $this->string(255)->notNull(),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180407_183505_all_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180407_183505_all_tables cannot be reverted.\n";

        return false;
    }
    */
}
