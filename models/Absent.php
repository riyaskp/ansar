<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "absent".
 *
 * @property int $id
 * @property int $employee_id
 * @property string $absent_type
 * @property string $holiday_type
 * @property string $start_date
 * @property string $end_date
 * @property string $place
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 */
class Absent extends \yii\db\ActiveRecord
{
    const ABSENT_TYPE =[
        'Holiday' => 'إجازة',
        'Course' => 'دورة',
        'Leave' => 'استئذان',
        'Mandate' => 'انتداب',
        'Append' => 'الحاق'
    ];

    const HOLDAY_TYPE =[
        'اعتياديه' => 'اعتياديه',
        'عرضية' => 'عرضية',
        'مرضية' => 'مرضية',
        'استثنائية' => 'استثنائية',
        'دراسية' => 'دراسية',
        'ميدانية' => 'ميدانية'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'absent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'absent_type', 'start_date', 'end_date'], 'required'],
            [['employee_id'], 'integer'],
            [['start_date', 'end_date', 'leave_type', 'holiday_type', 'course_type'], 'safe'],
            [['absent_type', 'holiday_type', 'place'], 'string', 'max' => 55],
            ['holiday_type', 'validateHolidayType'],
            ['leave_type', 'validateLeaveType'],
            ['course_type', 'validateCourseType'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateHolidayType($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->holiday_type && $this->absent_type != 'Holiday') {
                $this->addError($attribute, Yii::t('app','Select absent type Holiday.'));
            }
        }
    }

    public function validateLeaveType($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->holiday_type && $this->absent_type != 'Leave') {
                $this->addError($attribute, Yii::t('app','Select absent type Leave.'));
            }
        }
    }

    public function validateCourseType($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->holiday_type && $this->absent_type != 'Course') {
                $this->addError($attribute, Yii::t('app','Select absent type Course.'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'absent_type' => Yii::t('app', 'Absent Type'),
            'holiday_type' => Yii::t('app', 'Holiday Type'),
            'course_type' => Yii::t('app', 'Course Type'),
            'leave_type' => Yii::t('app', 'Leave Type'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'place' => Yii::t('app', 'Place'),
            'comment' => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    public function getReason()
    {
        switch ($this->absent_type) {
            case "Holiday":
                return self::HOLDAY_TYPE[$this->holiday_type];
                break;
            case "Course":
                return $this->course_type;
                break;
            case "Leave":
                return $this->leave_type;
                break;
            default:
                return null;
        }
    }
}
