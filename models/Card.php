<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "card".
 *
 * @property int $id
 * @property int $employee_id
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property string $updated_at
 */
class Card extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'start_date', 'end_date'], 'required'],
            [['employee_id'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveCard()
    {
        if(strtotime($this->end_date. '23:59:59')> time()){
            return true;
        } else {
            return false;
        }
    }

    public function getCardStatus(){
        if ($this->activeCard){
            return Yii::t('app', 'Yes');
        } else {
			return Yii::t('app', 'No');
		}
        
    }

    public function getRemainingDays(){
        if ($this->activeCard){
            $date1 = \DateTime::createFromFormat('Y-m-d',$this->end_date);
            $date2 = \DateTime::createFromFormat('Y-m-d',date('Y-m-d'));
            return $diff = $date2->diff($date1)->format("%a")+1;
        }
        return null;
    }
}
