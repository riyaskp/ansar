<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Absent;

/**
 * AbsentSearch represents the model behind the search form of `app\models\Absent`.
 */
class AbsentSearch extends Absent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employee_id'], 'integer'],
            [['absent_type', 'holiday_type', 'start_date', 'end_date', 'place', 'comment', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Absent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' =>false
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'end_date',
            ],
            'defaultOrder' => [
                'end_date' => SORT_DESC
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if($this->start_date){
            $query->andFilterWhere(['>=', 'start_date', $this->start_date]);
        }
        if($this->end_date){
            $query->andFilterWhere(['<=', 'end_date', $this->end_date]);
        }
        

        $query->andFilterWhere(['like', 'absent_type', $this->absent_type])
            ->andFilterWhere(['like', 'holiday_type', $this->holiday_type])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
