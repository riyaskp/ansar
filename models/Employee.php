<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $rank
 * @property string $specialist
 * @property string $office
 * @property string $joining_date
 * @property string $promotion_date
 * @property string $created_at
 * @property string $updated_at
 */
class Employee extends \yii\db\ActiveRecord
{
    const RANK_LIST=[
        'A-الرئيس الرقباء' => 'الرئيس الرقباء',
        'B-الرقيب الأول' => 'الرقيب الأول',
        'C-الرقيب' => 'الرقيب',
        'D-الوكيل الرقيب' => 'الوكيل الرقيب',
        'E-العريف' => 'العريف',
        'F-الجندي الأول' => 'الجندي الأول',
        'G-الجندي' => 'الجندي',
		'H-الموضف' => 'الموضف'
    ];
    const SPECIALIST_LIST=[
        'كاتب' => 'كاتب',
        'مراسل' => 'مراسل',
        'خدمات' => 'خدمات',
        'سائق' => 'سائق',
		'حاسب آلي' => 'حاسب آلي',
        'أخرى' => 'أخرى'		
    ];

    const OFFICE_LIST=[
        'اتصالات الافراد' => 'اتصالات الافراد',
        'الاستحقاقات' => 'الاستحقاقات',
        'الترقيات' => 'الترقيات',
        'التعيين والإعادة' => 'التعيين والإعادة',
        'التشكيلات' => 'التشكيلات',
        'النقل والالحاق' => 'النقل والالحاق',
        'إنهاء الخدمة' => 'إنهاء الخدمة',
        'الاجازات' => 'الاجازات',
        'السجلات والمحفوظات' => 'السجلات والمحفوظات',
        'الدورات والابتعاث' => 'الدورات والابتعاث',
        'الانتدابات' => 'الانتدابات'
    ];

    const SHIFT_LIST=[
        'دوام كامل' => 'دوام كامل',
        'صباحي' => 'صباحي',
        'مسائي' => 'مسائي',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'mobile', 'rank', 'specialist', 'office', 'joining_date', 'promotion_date', 'shift'], 'required'],
            [['joining_date', 'promotion_date', 'created_at', 'updated_at'], 'safe'],
            [['code', 'name', 'email', 'rank', 'specialist', 'office', 'shift'], 'string', 'max' => 55],
            ['code','unique'],
            [['mobile'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'mobile' => Yii::t('app', 'Mobile'),
            'email' => Yii::t('app', 'Email'),
            'rank' => Yii::t('app', 'Rank'),
            'specialist' => Yii::t('app', 'Specialist'),
            'office' => Yii::t('app', 'Office'),
            'shift' => Yii::t('app','Shift'),
            'joining_date' => Yii::t('app', 'Joining Date'),
            'promotion_date' => Yii::t('app', 'Promotion Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveAbsent()
    {
        return $this->hasOne(Absent::className(), ['employee_id' => 'id'])->andOnCondition('start_date<=date() AND end_date>=date()');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsents()
    {
        return $this->hasMany(Absent::className(), ['employee_id' => 'id']);
    }

    public function getAbsentStatus(){
        if ($this->activeAbsent){
            return Yii::t('app', 'Yes');
        } else {
			return Yii::t('app', 'No');
		}
        
    }

    public function getAbsentType(){
        if ($this->activeAbsent){
            return $this->activeAbsent->absent_type;
        }
        return null;
    }
	
	public function getHolidayType(){
        if ($this->activeAbsent){
            return $this->activeAbsent->holiday_type;
        }
        return null;
    }
	
	public function getReason() {
		if ($this->absentType) {
			switch ($this->absentType) {
				case 'Holiday':
					return \app\models\Absent::HOLDAY_TYPE[$this->activeAbsent->holiday_type];
					break;
				case 'Course':
					return $this->activeAbsent->course_type;
					break;
				case 'Leave':
					return $this->activeAbsent->leave_type;
					break;
				default:
					return null;
			}
		} else {
			return null;
		}
	}

    public function getRemainingDays(){
        if ($this->activeAbsent){
            $date1 = \DateTime::createFromFormat('Y-m-d',$this->activeAbsent->end_date);
            $date2 = \DateTime::createFromFormat('Y-m-d',date('Y-m-d'));
            return $diff = $date2->diff($date1)->format("%a")+1;
        }
        return null;
    }
}
