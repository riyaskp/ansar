<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Card;

/**
 * CardSearch represents the model behind the search form of `app\models\Card`.
 */
class CardSearch extends Card
{
    public $code;
    public $name;
    public $rank;
    public $office;
    public $specialist;
    public $cardStatus;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employee_id'], 'integer'],
            [['code', 'name', 'rank', 'office', 'specialist', 'cardStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Card::find();

        // add conditions that should always apply here
        $query->joinWith(['employee']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->setSort([
            'attributes' => [
                'rank' => [
                    //'asc'=>['rank'=>SORT_ASC, new \yii\db\Expression('IFNULL(absent.end_date, "")=""'), 'absent.end_date'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC],
					'asc'=>['employee.rank'=>SORT_ASC, 'employee.promotion_date'=>SORT_ASC, 'employee.joining_date'=>SORT_ASC, 'employee.code'=>SORT_ASC],
                    //'desc'=>['rank'=>SORT_DESC, new \yii\db\Expression('IFNULL(absent.end_date, "")=""'), 'absent.end_date'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC]
					'desc'=>['employee.rank'=>SORT_DESC, 'employee.promotion_date'=>SORT_ASC, 'employee.joining_date'=>SORT_ASC, 'employee.code'=>SORT_ASC]
                ],
                'remainingDays' => [
                    'asc'=>[new \yii\db\Expression('IFNULL(end_date, "")=""'), 'end_date'=>SORT_ASC, 'employee.rank'=>SORT_ASC, 'employee.promotion_date'=>SORT_ASC, 'employee.joining_date'=>SORT_ASC],
                    'desc'=>[new \yii\db\Expression('IFNULL(end_date, "")=""'), 'end_date'=>SORT_DESC, 'employee.rank'=>SORT_ASC, 'employee.promotion_date'=>SORT_ASC, 'employee.joining_date'=>SORT_ASC],
                    'label'=>Yii::t('app','Remaining Days')
                ]
            ],
            'defaultOrder' => [
                'remainingDays' => SORT_ASC
            ]
        ]);
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'employee.code' => $this->code,
            'employee.name' => $this->name,
            'employee.rank' => $this->rank,
            'employee.office' => $this->office,
            'employee.specialist' => $this->specialist,
        ]);
        if($this->cardStatus=='yes'){
            $query->andWhere(
                'end_date>=date()'
            );
        }else if($this->cardStatus=='no'){
            $query->andWhere(
                'end_date<date() OR end_date IS NULL'
            );
        }
        return $dataProvider;
    }
}
