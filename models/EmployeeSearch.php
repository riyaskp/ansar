<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employee;

/**
 * EmployeeSearch represents the model behind the search form of `app\models\Employee`.
 */
class EmployeeSearch extends Employee
{
    public $remainingDays;
    public $absentStatus;
    public $absentType;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['code', 'name', 'mobile', 'email', 'rank', 'specialist', 'office', 'shift', 'joining_date', 'promotion_date', 'remainingDays', 'absentStatus', 'absentType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employee::find()->joinWith(['activeAbsent']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' =>false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'joining_date' => $this->joining_date,
            'promotion_date' => $this->promotion_date,
            'code' => $this->code,
            'mobile' => $this->mobile,
            'name' => $this->name,
            'email' => $this->email,
            'rank' => $this->rank,
            'specialist' => $this->specialist,
            'office' => $this->office,
            'shift' => $this->shift,
        ]);

        if($this->absentStatus=='yes'){
            $query->andWhere(
                'absent.start_date<=date() AND absent.end_date>=date()'
            );
        }else if($this->absentStatus=='no'){
            $query->andWhere(
                'absent.end_date<date() OR absent.end_date IS NULL'
            );
        }
        if($this->absentType) {
            $query->andFilterWhere([
                'absent.absent_type' => $this->absentType,
            ]);
        }
        $dataProvider->setSort([
            'attributes' => [
                'rank' => [
                    //'asc'=>['rank'=>SORT_ASC, new \yii\db\Expression('IFNULL(absent.end_date, "")=""'), 'absent.end_date'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC],
					'asc'=>['rank'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC, 'code'=>SORT_ASC],
                    //'desc'=>['rank'=>SORT_DESC, new \yii\db\Expression('IFNULL(absent.end_date, "")=""'), 'absent.end_date'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC]
					'desc'=>['rank'=>SORT_DESC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC, 'code'=>SORT_ASC]
                ],
                'remainingDays' => [
                    'asc'=>[new \yii\db\Expression('IFNULL(absent.end_date, "")=""'), 'absent.end_date'=>SORT_ASC, 'rank'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC],
                    'desc'=>[new \yii\db\Expression('IFNULL(absent.end_date, "")=""'), 'absent.end_date'=>SORT_DESC, 'rank'=>SORT_ASC, 'promotion_date'=>SORT_ASC, 'joining_date'=>SORT_ASC],
                    'label'=>Yii::t('app','Remaining Days')
                ]
            ],
            'defaultOrder' => [
                'remainingDays' => SORT_ASC
            ]
        ]);
        return $dataProvider;
    }
}
