<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Absent */

$this->title = Yii::t('app', 'Create Absent');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employee'), 'url' => ['employee/view', 'id'=>$model->employee_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
