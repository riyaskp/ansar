<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Absent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-6 absent-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'absent_type')->dropDownList($model::ABSENT_TYPE, ['prompt'=>Yii::t('app', 'Absent type')]); ?>

    <div id="holiday-section" style="<?= $model->absent_type=='Holiday'?'':'display:none;' ?>">
    <?= $form->field($model, 'holiday_type')->dropDownList($model::HOLDAY_TYPE, ['prompt'=>Yii::t('app', 'Holiday type')]) ?>
    </div>
    <div id="course-section"  style="<?= $model->absent_type=='Course'?'':'display:none;' ?>">
    <?= $form->field($model, 'course_type')->textInput(['maxlength' => true]) ?>
    </div>

    <div id="leave-section"  style="<?= $model->absent_type=='Leave'?'':'display:none;' ?>">
    <?= $form->field($model, 'leave_type')->textInput(['maxlength' => true]) ?>
    </div>
    

    <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => Yii::t('app', 'Select Starting Date')],
            'language' => 'ar',
            'removeButton' => false,
            //'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) 
    ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => Yii::t('app', 'Select End Date')],
            'language' => 'ar',
            'removeButton' => false,
            //'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) 
    ?>
    <div id="place-section">
    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
    </div>
    <?= $form->field($model, 'comment')->textArea() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $('#absent-absent_type').on('change', function(){
        if($(this).val()=="Leave"){
            $( "#place-section").hide();
        }else{
            $( "#place-section").show();
            $( "#place-section label" ).first().text('موقع '+$('#absent-absent_type option:selected').text());
        }
        if($(this).val()=="Course"){
            $('#holiday-section').hide();
            $('#course-section').show();
            $('#leave-section').hide();
            $('#absent-leave_type').val('');
            $('#absent-holiday_type').val('');
        }else if($(this).val()=="Holiday"){
            $('#holiday-section').show();
            $('#course-section').hide();
            $('#leave-section').hide();
            $('#absent-leave_type').val('');
            $('#absent-course_type').val('');
        }else if($(this).val()=="Leave"){
            $('#holiday-section').hide();
            $('#course-section').hide();
            $('#leave-section').show();
            $('#absent-holiday_type').val('');
            $('#absent-course_type').val('');
        }
        else{
            $('#holiday-section').hide();
            $('#course-section').hide();
            $('#leave-section').hide();
            $('#absent-leave_type').val('');
            $('#absent-holiday_type').val('');
            $('#absent-course_type').val('');
        }
    });
JS;
$this->registerJs($script);
?>
