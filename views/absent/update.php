<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Absent */

$this->title = Yii::t('app', 'Update Absent: {nameAttribute}', [
    'nameAttribute' => $model->absent_type,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employee'), 'url' => ['employee/view', 'id'=>$model->employee_id]];
$this->params['breadcrumbs'][] = ['label' => $model::ABSENT_TYPE[$model->absent_type], 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="absent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
