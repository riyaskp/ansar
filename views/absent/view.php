<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Absent */

$this->title = Yii::t('app','Absent ').$model::ABSENT_TYPE[$model->absent_type];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employee'), 'url' => ['employee/view', 'id'=>$model->employee_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'employee.code',
            'employee.name',
            [
                'attribute' => 'absent_type',
                'value' => $model::ABSENT_TYPE[$model->absent_type]
            ],
            [
                'label' => Yii::t('app','Reason'),
                'attribute' => 'reason',
            ],
            [
                'attribute' => 'start_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'attribute' => 'end_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            'place',
            'comment'
        ],
    ]) ?>

</div>
