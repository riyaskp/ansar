<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employee'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-view">

    <h1 class="no-print"><?= Html::encode($this->title) ?></h1>

    <p class="no-print">
        <?= Html::a(Yii::t('app', 'Create Absent'), ['create-absent', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Print'), '#', ['class' => 'btn btn-primary', 'id'=>'print-user-detail']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'id' => 'user-detail-grid',
        'attributes' => [
            //'id',
            'code',
            [
                'attribute' => 'rank',
                'value' => $model::RANK_LIST[$model->rank]
            ],
            'name',
            [
                'attribute' => 'mobile',
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ],
            [
                'attribute' => 'email',
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ],
            [
                'attribute' => 'specialist',
                'value' => $model::SPECIALIST_LIST[$model->specialist],
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ],
            [
                'attribute' => 'office',
                'value' => $model::OFFICE_LIST[$model->office],
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ],
            [
                'attribute' => 'shift',
                'value' => $model::SHIFT_LIST[$model->shift],
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ],
            [
                'attribute' => 'joining_date',
                'format' => ['date', 'php:d-m-Y'],
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ],
            [
                'attribute' => 'promotion_date',
                'format' => ['date', 'php:d-m-Y'],
                'contentOptions' => ['class' => 'no-print'],
                'captionOptions' => ['class' => 'no-print'],
            ]
        ],
    ]) ?>

</div>

<div class="absent-index">

<h1><?= Html::encode(Yii::t("app","Absent")) ?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	'layout' => '{items}\n{pager}',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', 'header' => Yii::t('app', 'Number')],
        [
            'attribute' => 'absent_type',
            'value' => function ($model) {
                return $model::ABSENT_TYPE[$model->absent_type];
            },
            'filter' => \app\models\Absent::ABSENT_TYPE
        ],
        [
            'label' => Yii::t('app','Reason'),
            'attribute' => 'reason'
        ],
        [
            'attribute' => 'start_date',
            'format' => ['date', 'php:d-m-Y'],
            'filter' => \kartik\date\DatePicker::widget([
                'model'=>$searchModel,
                'attribute'=>'start_date',
                'language' => 'ar',
                'type' => \kartik\date\DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]),
        ],
        [
            'attribute' => 'end_date',
            'format' => ['date', 'php:d-m-Y'],
            'filter' => \kartik\date\DatePicker::widget([
                'model'=>$searchModel,
                'attribute'=>'end_date',
                'language' => 'ar',
                'type' => \kartik\date\DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]),
        ],
        'place',
        [
            'label' => 'المده',
            'value' => function ($model) {
                $day1 = new \DateTime($model->start_date);
                $day2 = new \DateTime($model->end_date);
                $interval = round(abs($day2->format('U') - $day1->format('U')) / (60*60*24));
                return $interval+1;
            },
            'filter' => \app\models\Absent::ABSENT_TYPE
        ],
        //'comment',
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'no-print'],
            'header' => 'Actions',
            'headerOptions' => ['style' => 'color:#337ab7', 'class'=>'no-print'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['absent/view', 'id'=>$model->id], [
                                'title' => Yii::t('app', 'absent-view'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['absent/update', 'id'=>$model->id], [
                                'title' => Yii::t('app', 'absent-update'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['absent/delete', 'id'=>$model->id], [
                                'title' => Yii::t('app', 'absent-delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method'  => 'post',
                    ]);
                }

            ],
    
        ],
    ],
]); ?>
</div>
<?php
$script = <<< JS
    $('#print-user-detail').on('click', function(){
        $('.filters').addClass('no-print');
        //var source=$( '#user-detail-grid' ).clone( true );
        //source.find("tr").eq(3).nextAll().remove();
        window.print();
    });
JS;
$this->registerJs($script);
?>

