<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-6 employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rank')->dropDownList($model::RANK_LIST, ['prompt'=>Yii::t('app', 'Select rank')]); ?>

    <?= $form->field($model, 'specialist')->dropDownList($model::SPECIALIST_LIST, ['prompt'=>Yii::t('app', 'Select specialist')]); ?>

    <?= $form->field($model, 'office')->dropDownList($model::OFFICE_LIST, ['prompt'=>Yii::t('app', 'Select office')]); ?>

    <?= $form->field($model, 'shift')->dropDownList($model::SHIFT_LIST, ['prompt'=>Yii::t('app', 'Select shift')]); ?>

    <?= $form->field($model, 'joining_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => Yii::t('app', 'Select Joining Date')],
            'language' => 'ar',
            'removeButton' => false,
            //'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) 
    ?>

    <?= $form->field($model, 'promotion_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => Yii::t('app', 'Select Promotion Date')],
            'language' => 'ar',
            'removeButton' => false,
            //'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
