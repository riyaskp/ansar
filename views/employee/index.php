<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
#print-title {
   display : none;
}

@media print {
    #print-title {
       display : block;
    }
	a[href]:after {
		content: " (" attr(href) ")";
    }
	a[href]:after {
		content: none !important;
	}
}

.dropdown-menu > li:hover .dropdown-menu-item,
.dropdown-menu > li:focus .dropdown-menu-item{
    background-color: #f5f5f5;
    color: #262625;
}
.dropdown-menu > li > .dropdown-menu-item.checkbox {
    margin: 0;
    font-weight: normal;
}
.dropdown-menu > li > .dropdown-menu-item.checkbox input {
    display: none;
}
</style>
<div class="employee-index">

    <h1 class="no-print"><?= Html::encode($this->title) ?></h1>
	<h1 class="text-center" id="print-title"><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Employee'), ['create'], ['class' => 'btn btn-success no-print']) ?>
        <?= Html::a(Yii::t('app', 'Card'), ['card/index'], ['class' => 'btn btn-success no-print']) ?>

			<div class="form-inline no-print">
				<input type="text" class="form-control" id="print-employee-list-name" placeholder="<?= Yii::t('app','Print file heading') ?>">
                <button type="button" class="btn btn-success" , id="print-employee-list"><?= Yii::t('app', 'Print') ?></button>
            </div>
			<div class="btn-group no-print">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button>
				<ul class="dropdown-menu">
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox" name="1" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Number') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox" name="2" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('rank') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="3" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('code') ?>
                        </label>
                    </li> 
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="4" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('name') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="5" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('specialist') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="6" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('office') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="7" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('shift') ?>
                        </label>
                    </li>
                    <!--<li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="8" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('joining_date') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="9" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $searchModel->getAttributeLabel('promotion_date') ?>
                        </label>
                    </li>-->
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="8" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Absent Status') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="9" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Absent Type') ?>
                        </label>
                    </li>
					<li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="10" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'ٌReasons') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="11" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Remaining Days') ?>
                        </label>
                    </li> 
				</ul>
			</div>			
        
    </p>
	<div class="summary">مجموع <b><?= $dataProvider->getTotalCount(); ?></b> <?= $dataProvider->getTotalCount()==1?'مُدخل':'مُدخلات'; ?>.</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'layout' => '{items} {pager}',
		'id' => 'employee-list',
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
				'header' => Yii::t('app', 'Number'),
            ],
            //'id',
            [
                'attribute' => 'rank',
                'value' => function ($model) {
                    return \app\models\Employee::RANK_LIST[$model->rank];
                },
                'filter' => \app\models\Employee::RANK_LIST
            ],
            'code',
            'name',
            //'mobile',
            //'email:email',
            [
                'attribute' => 'specialist',
                'value' => function ($model) {
                    return \app\models\Employee::SPECIALIST_LIST[$model->specialist];
                },
                'filter' => \app\models\Employee::SPECIALIST_LIST
            ],
            [
                'attribute' => 'office',
                'value' => function ($model) {
                    return \app\models\Employee::OFFICE_LIST[$model->office];
                },
                'filter' => \app\models\Employee::OFFICE_LIST
            ],
            [
                'attribute' => 'shift',
                'value' => function ($model) {
                    return \app\models\Employee::SHIFT_LIST[$model->shift];
                },
                'filter' => \app\models\Employee::SHIFT_LIST
            ],
            /*[
                'attribute' => 'joining_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'attribute' => 'promotion_date',
                'format' => ['date', 'php:d-m-Y']
            ],*/
            [
                'attribute' => 'absentStatus',
                'label' => Yii::t('app', 'Absent Status'),
                'filter' => ['yes'=>Yii::t('app', 'Yes'), 'no'=>Yii::t('app', 'No')]
            ],
            [
                'label' => Yii::t('app', 'Absent Type'),
                'attribute' => 'absentType',
                'value' => function ($model) {
                    return $model->absentType?\app\models\Absent::ABSENT_TYPE[$model->absentType]:null;
                },
                'filter' => \app\models\Absent::ABSENT_TYPE
            ],
			[
                'label' => Yii::t('app', 'ٌReasons'),
                'value' => function ($model) {
                    if($model->absentType!="Holiday" && $model->absentType!="Course" && $model->absentType!="Leave") {
                        return $model->absentType?\app\models\Absent::ABSENT_TYPE[$model->absentType]:null;
                    } else {
                        return $model->reason;
                    }
                },
                'attribute' => 'reason',
            ],
            [
                'label' => Yii::t('app', 'Remaining Days'),
                'attribute' => 'remainingDays',
            ],
            //'created_at',
            //'updated_at',

            [
            'headerOptions' => ['class'=>'no-print'],
			'class' => 'yii\grid\ActionColumn',
			'contentOptions' => ['class' => 'no-print'],
			],
        ],
    ]); ?>
</div>

<?php
$script = <<< JS
    $('#print-employee-list').on('click', function(){
		$('#print-title').html($('#print-employee-list-name').val());
        /*$('.filters').addClass('no-print');
        $('.summary').addClass('no-print');*/
        window.print();
    });
	$(function(){
		$( '.dropdown-menu li' ).on( 'click', function( event ) {
			var checkbox = $(this).find('.checkbox');
			if (!checkbox.length) {
				return;
			}
			var input = checkbox.find('input');
			var icon = checkbox.find('span.glyphicon');
            var index = checkbox.find('input').attr('name');
            console.log(index);
			if (input.is(':checked')) {
				input.prop('checked',false);
				icon.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                $('#employee-list table th:nth-child('+index+')').addClass('no-print');
                $('#employee-list table td:nth-child('+index+')').addClass('no-print');
			} else {
				input.prop('checked',true);
				icon.removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                $('#employee-list table th:nth-child('+index+')').removeClass('no-print');
                $('#employee-list table td:nth-child('+index+')').removeClass('no-print');
			}
			return false;
		}); 
	});
JS;
$this->registerJs($script);
?>
