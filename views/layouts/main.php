<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html dir='rtl' xmlns="http://www.w3.org/1999/xhtml" xml:lang="ar" lang="ar">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css" media="print">
		body {
			font-size:16px;
			width:100%;
			height : 100%;
		}
		.filters {
			display: none;
		}
		.summary {
			display: none;
		}
		html {
			width:100%;
		}
        .no-print { display: none; }
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top no-print',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left no-print'],
        'items' => [
            ['label' => 'الرئيسيه', 'url' => ['/employee/index']],
            Yii::$app->user->isGuest ? (
                ['label' => 'دخول', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'خروج (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="col-sm-12 no-print">
            <div class="col-sm-3">
                <img src="<?php echo Yii::getAlias('@web').'/images/logo1.png'; ?>">
            </div>
            <div class="col-sm-6">
                <div style="margin-top: 75px;">
                    <h2 style="text-align: center;">​قاعدة الملك عبدالله الجوية بالقطاع الغربي</h2>
                    <h4 style="text-align: center;">جناح الادارة / شؤون الافراد</h4>
                </div>
            </div>
            <div class="col-sm-3">
                <img style="width: 85%;" src="<?php echo Yii::getAlias('@web').'/images/logo.png'; ?>">
            </div>
        </div>
        <div class="no-print">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        </div>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer no-print">
    <div class="container">
        <p class="pull-left">&copy; فكره واعداد الرئيس الرقباء/عميربن سعيد الجدعاني <?= date('Y') ?></p>
		
        <p class="pull-right"><a href="http://youbon.in" target="_blank"></a>برنامج خاص بشؤون الافراد</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
