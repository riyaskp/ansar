<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Employee;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Card */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-6 card-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'employee_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Employee::find()->all(), 'id', 'code'),
        'language' => 'ar',
        'options' => ['placeholder' => Yii::t('app', 'Select a employee ...')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => Yii::t('app', 'Select Starting Date')],
            'language' => 'ar',
            'removeButton' => false,
            //'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) 
    ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => Yii::t('app', 'Select End Date')],
            'language' => 'ar',
            'removeButton' => false,
            //'convertFormat' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) 
    ?>
    <?php /*<?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'end_date')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>*/ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
