<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Employee;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cards');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
#print-title {
   display : none;
}

@media print {
    #print-title {
       display : block;
    }
	a[href]:after {
		content: " (" attr(href) ")";
    }
	a[href]:after {
		content: none !important;
	}
}

.dropdown-menu > li:hover .dropdown-menu-item,
.dropdown-menu > li:focus .dropdown-menu-item{
    background-color: #f5f5f5;
    color: #262625;
}
.dropdown-menu > li > .dropdown-menu-item.checkbox {
    margin: 0;
    font-weight: normal;
}
.dropdown-menu > li > .dropdown-menu-item.checkbox input {
    display: none;
}
</style>
<div class="card-index">

    <h1 class="no-print"><?= Html::encode($this->title) ?></h1>
	<h1 class="text-center" id="print-title"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Card'), ['create'], ['class' => 'btn btn-success no-print']) ?>
        <div class="form-inline no-print">
				<input type="text" class="form-control" id="print-card-list-name" placeholder="<?= Yii::t('app','Print file heading') ?>">
                <button type="button" class="btn btn-success" , id="print-card-list"><?= Yii::t('app', 'Print') ?></button>
            </div>
			<div class="btn-group no-print">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button>
				<ul class="dropdown-menu">
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox" name="1" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Number') ?>
                        </label>
                    </li>

                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox" name="2" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $employeeModel->getAttributeLabel('rank') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="3" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $employeeModel->getAttributeLabel('code') ?>
                        </label>
                    </li> 
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="4" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $employeeModel->getAttributeLabel('name') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="5" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $employeeModel->getAttributeLabel('office') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="6" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= $employeeModel->getAttributeLabel('specialist') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="7" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Card Status') ?>
                        </label>
                    </li>
                    <li>
                        <label class="dropdown-menu-item checkbox">
                            <input type="checkbox"  name="8" checked />
                            <span class="glyphicon glyphicon-check"></span>
                            <?= Yii::t('app', 'Remaining Days') ?>
                        </label>
                    </li> 
				</ul>
			</div>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'card-list',
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
				'header' => Yii::t('app', 'Number'),
            ],
            [
                'attribute' => 'rank',
                'label' => Yii::t('app', 'Rank'),
                'value' => function ($model) {
                    return \app\models\Employee::RANK_LIST[$model->employee->rank];
                },
                'filter' => \app\models\Employee::RANK_LIST
            ],
            //'id',
            [
                'attribute' => 'code',
                'label' => Yii::t('app', 'Code'),
                'value' => 'employee.code',
            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('app', 'Name'),
                'value' => 'employee.name',
                //'filter' => \app\models\Employee::RANK_LIST
            ],
            
            [
                'attribute' => 'office',
                'label' => Yii::t('app', 'Office'),
                'value' => 'employee.office',
                'filter' => \app\models\Employee::OFFICE_LIST
            ],
            [
                'attribute' => 'specialist',
                'label' => Yii::t('app', 'Specialist'),
                'value' => 'employee.specialist',
                'filter' => \app\models\Employee::SPECIALIST_LIST
            ],
            [
                'attribute' => 'cardStatus',
                'label' => Yii::t('app', 'Card Status'),
                'filter' => ['yes'=>Yii::t('app', 'Yes'), 'no'=>Yii::t('app', 'No')]
            ],
            [
                'label' => Yii::t('app', 'Remaining Days'),
                'attribute' => 'remainingDays',
            ],
            /*[
                'attribute' => 'start_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'attribute' => 'end_date',
                'format' => ['date', 'php:d-m-Y']
            ],*/
            //'created_at',
            //'updated_at',

            [
                'headerOptions' => ['class'=>'no-print'],
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'no-print'],
            ],
        ],
    ]); ?>
</div>
<?php
$script = <<< JS
    $('#print-card-list').on('click', function(){
		$('#print-title').html($('#print-card-list-name').val());
        /*$('.filters').addClass('no-print');
        $('.summary').addClass('no-print');*/
        window.print();
    });
	$(function(){
		$( '.dropdown-menu li' ).on( 'click', function( event ) {
			var checkbox = $(this).find('.checkbox');
			if (!checkbox.length) {
				return;
			}
			var input = checkbox.find('input');
			var icon = checkbox.find('span.glyphicon');
            var index = checkbox.find('input').attr('name');
            console.log(index);
			if (input.is(':checked')) {
				input.prop('checked',false);
				icon.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                $('#card-list table th:nth-child('+index+')').addClass('no-print');
                $('#card-list table td:nth-child('+index+')').addClass('no-print');
			} else {
				input.prop('checked',true);
				icon.removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                $('#card-list table th:nth-child('+index+')').removeClass('no-print');
                $('#card-list table td:nth-child('+index+')').removeClass('no-print');
			}
			return false;
		}); 
	});
JS;
$this->registerJs($script);
?>